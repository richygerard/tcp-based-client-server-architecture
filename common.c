#include "common.h"
#include <stdlib.h>
#include "server.h"
#include "client.h"

int read_config_file(const int type, void *buffer)
{

    FILE *fp = NULL;
    struct client_info *client_args_ptr = NULL;
    struct server_info *server_args_ptr = NULL;
    char server_ip[INET_ADDRSTRLEN] = {0};


    switch(type)
    {
      case CLIENT:
          fp = fopen("client.in", "r");
          client_args_ptr = (struct client_info *)buffer;

          if(fp == NULL)
          {
              fprintf(stderr, "Could not open the file \"client.in\".\n");
              exit(EXIT_FAILURE);
          }
          if(fscanf(fp, "%s", server_ip) <= 0)
          {
              fprintf(stderr, "IP address of server missing.\n");
              return -1;
          }

#ifdef DEBUG
          printf("server_ip is : %s\n", server_ip);
#endif

          if(inet_pton(AF_INET, server_ip, &client_args_ptr->server_addr) != 1) {
              fprintf(stderr, "Error parsing IP address of server.\n");
              return -1;
          }
          if(fscanf(fp, "%hu", &client_args_ptr->server_port) <= 0)
          {
              fprintf(stderr, "Port number of server missing.\n");
              return -1;
          }
          if(fscanf(fp, "%s", client_args_ptr->filename) <= 0)
          {	
              fprintf(stderr, "File name is missing.\n");
              return -1;

          }
          if(fscanf(fp, "%u", &client_args_ptr->rx_window_size) <= 0)
          {
              fprintf(stderr, "Receiving sliding-window size is missing.\n");
              return -1;
          }
          if(fscanf(fp, "%u", &client_args_ptr->random_seed) <= 0)
          {
              fprintf(stderr, "Random generator seed value is missing.\n");
              return -1;
          }
          if(fscanf(fp, "%f", &client_args_ptr->loss_probability) <= 0)
          {
              fprintf(stderr, "Probability of datagram loss is missing.\n");
              return -1;
          }

          if((client_args_ptr->loss_probability < 0.0) ||
             (client_args_ptr->loss_probability > 1.0)){
              printf("%f is not a valid loss probability value.\n",
                     client_args_ptr->loss_probability);
              return -1;
          }

          if(fscanf(fp, "%u", &client_args_ptr->mean_mu) <= 0)
          {
              fprintf(stderr, "Mu values is missing.\n");
              return -1;
          }

#ifdef DEBUG
          printf("\nContents of client.in:\n");
          printf("----------------------\n");
          printf("Server address : %s\n", inet_ntop(AF_INET, &client_args_ptr->server_addr, server_ip, INET_ADDRSTRLEN));
          printf("Server port : %hu\n", client_args_ptr->server_port);
          printf("Filename : %s\n", client_args_ptr->filename);
          printf("Receiver Window Size : %u\n", client_args_ptr->rx_window_size);
          printf("Random seed : %u\n", client_args_ptr->random_seed);
          printf("Loss Probability : %f\n", client_args_ptr->loss_probability);
          printf("The mean 'u' in millisecs : %u\n\n", client_args_ptr->mean_mu);
#endif

          fclose(fp);
          break;

      case SERVER:
          fp = fopen("server.in", "r");
          server_args_ptr = (struct server_info *)buffer;

          if(fp == NULL)
          {
              fprintf(stderr, "Could not open the file \"server.in\".\n");
              exit(EXIT_FAILURE);
          }

          if(fscanf(fp, "%hu", &server_args_ptr->ftp_port) <= 0)
          {
              fprintf(stderr, "Port number is missing.\n");
              return -1;
          }
          if(fscanf(fp, "%u", &server_args_ptr->tx_window_size) <= 0) 
          {
              fprintf(stderr, "Sending sliding-window size missing.\n");
              return -1;
          }

#ifdef DEBUG
          printf("\nContents of server.in:\n");
          printf("----------------------\n");
          printf("FTP port : %hu\n", server_args_ptr->ftp_port);
          printf("Maximum sending sliding-window size : %u\n\n", server_args_ptr->tx_window_size);
#endif

          fclose(fp);
          break;

    }
    return 0;
}

#ifdef DEBUG
void dump_packet_contents(struct reliable_udp_packet *pkt)
{
    printf("------------------------------------");
    switch(pkt->type)
    {
      case SYN_MSG:
          printf("\nDEBUG: Packet type : SYN_MSG\n");
          break;

      case SYN_ACK_MSG:
          printf("\nDEBUG: Packet type : SYN_ACK_MSG\n");
          break;

      case DATA_ACK_MSG:
          printf("\nDEBUG: Packet type : DATA_ACK_MSG\n");
          break;

      case DATA_PCKT:
          printf("\nDEBUG: Packet type : DATA_PCKT\n");
          break;

      case FIN_MSG:
          printf("\nDEBUG: Packet type : FIN_MSG\n");
          break;

      case FIN_ACK_MSG:
          printf("\nDEBUG: Packet type : FIN_ACK_MSG\n");
          break;

      case PROBE_WIN_MSG:
          printf("\nDEBUG: Packet type : PROBE_WIN_MSG\n");
          break;
    }

    printf("DEBUG: Sequence number: %u\n", pkt->sequence);
    printf("DEBUG: Window size: %u\n", pkt->window);
    printf("DEBUG: Timerstamp: %u\n", pkt->timestamp);
    printf("DEBUG: Payload: ");
    fflush(stdout);
    write(1, pkt->payload, pkt->payload_len);
    printf("\n");
    printf("------------------------------------\n");
}

#endif
