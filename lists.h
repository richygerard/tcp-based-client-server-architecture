struct list_head {
    struct list_head *next, *prev;
};

void INIT_LIST_HEAD(struct list_head *);

void __list_add(struct list_head *, struct list_head *, struct list_head *);

void __list_del(struct list_head *, struct list_head *);

void list_add(struct list_head *, struct list_head *);

void list_add_tail(struct list_head *, struct list_head *);

int list_empty(const struct list_head *);

void list_del(struct list_head *);

#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)

#define container_of(ptr, type, member) ({                      \
        (type *)( (char *)ptr - offsetof(type,member) );})

/* Iterate through the list */
#define list_for_each(pos, head) \
    for (pos = (head)->next; pos != (head); pos = pos->next)

#define list_entry(ptr, type, member) \
    container_of(ptr, type, member)

