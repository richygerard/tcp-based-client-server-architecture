#ifndef __SERVER_H__
#define __SERVER_H__

// #include "unp.h"
#include "unpifiplus.h"
#include "common.h"
#include "lists.h"

struct server_info
{
    in_port_t ftp_port;
    unsigned tx_window_size;
};

struct server_sock_record
{
    int     sockfd;
    struct  in_addr interface_addr;
    struct  in_addr network_mask;
    struct  in_addr subnet_addr;
    struct  list_head next_sock_record;
};

struct child_info {
    pid_t pid;
    struct sockaddr_in client_info;
    struct list_head next_child;
};

struct server_params {
        struct rtt_info rtt_value;
        struct rtt_info persist_rtt_value;
        unsigned sequence;
        unsigned window;
        unsigned last_ack;
        unsigned last_seq;
        unsigned receiver_window;
        unsigned congestion_window;
        unsigned ssthresh;
        struct reliable_udp_packet duplicate;
        char persist_mode;
        char cong_avoid;
        sigjmp_buf jmpbuf;
        sigjmp_buf persist_mode_jmpbuf;
};

enum {
    LOCAL,
    SAME_SUBNET,
    DIFFERENT_SUBNET
};

int bind_interfaces(struct ifi_info *, in_port_t);

void handle_incoming_connections(int);

void handle_client_request(struct server_sock_record *);

void service_client(struct sockaddr_in *, char *);

void transfer_file(int, char *);
#endif

