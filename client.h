#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "unpifiplus.h"
#include "common.h"

struct client_info
{
    struct in_addr server_addr;
    in_port_t server_port;
    char filename[100];
    unsigned rx_window_size;
    unsigned random_seed;
    float loss_probability;
    unsigned mean_mu;
};

struct client_params {
    unsigned sequence;
    unsigned window;
    unsigned last_ack;
    unsigned last_seq;
    int sockfd;
    struct reliable_udp_packet duplicate;
    int is_same_subnet;
    struct in_addr client_addr;
    struct in_addr server_addr;
    sigjmp_buf jmpbuf;
    struct rtt_info rtt_value;
};

void receive_file(int, struct sockaddr_in *, char *);

#endif
