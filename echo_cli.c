/* HEADER FILES */
#include	"unp.h"

/* Str_Client Function Defnition */
void str_client(FILE *fp, int sockfd,int fd)
{
	int maxfdp1,a;
	fd_set rset;
	char sendline[MAXLINE], recvline[MAXLINE];
	char msg[]="\n Data Typed is echoed \n";
	char msg1[]="\n Client Termination : Socket Read Error\n";

	FD_ZERO(&rset);								/* Implementing Select for the Time Function */
	for ( ; ; ) 
	{
		FD_SET(fileno(fp), &rset);
		FD_SET(sockfd, &rset);
		maxfdp1 = max(fileno(fp), sockfd) + 1;
		Select(maxfdp1, &rset, NULL, NULL, NULL);

		if (FD_ISSET(sockfd, &rset))					/* Listening to Socket */
		{	/* socket is readable */
			if (Readline(sockfd, recvline, MAXLINE) < 0)
				a=write(fd,msg,sizeof(msg));
			Fputs(recvline, stdout);
		}

		if (FD_ISSET(fileno(fp), &rset)) 				/* Listening to Input Stream */
		{  /* input is readable */
			if (Fgets(sendline, MAXLINE, fp) == NULL)
				return;		/* all done */
			Writen(sockfd, sendline, strlen(sendline));
				a=write(fd,msg,sizeof(msg));
		}
	}	


}

/* Main Function */
int main(int argc, char **argv)
{
	int sockfd,i;
	int fd=atoi(argv[2]);

	int maxfdp1;
	fd_set rset;
	char sendline[MAXLINE], recvline[MAXLINE];
	struct sockaddr_in servaddr;
	char buf2[MAXLINE]="",c;
	char msg1[]="\n Socket is Created\n";
	char msg2[]="\n STATUS : \n";
	char msg3[]="\n Connection Terminated\n";
	char msg4[]="\n Connection Established\n";
	printf("\n----------------------------- ");		
	printf("\n   CLIENT - TCP ECHO CLIENT ");
	printf("\n----------------------------- \n");	

										/*Writing the message to the client*/
	strcpy(buf2,"\n Client - TCP Echo Client Invoked\n");

	i=write(fd, buf2, sizeof (buf2));

	i=write(fd, msg2, sizeof (msg2));

	if (argc != 3)
		err_quit("usage: tcpcli <IPaddress>");
	
	printf("\nData you type will be echoed \n\n");

	sockfd = Socket(AF_INET, SOCK_STREAM, 0);				/* Socket Creation */
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(49391);

	i=write(fd, msg1, sizeof (msg1));	

	Inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
	Connect(sockfd, (SA *) &servaddr, sizeof(servaddr));			/* Socket Connection */
	
	i=write(fd, msg4, sizeof (msg4));

	str_client(stdin, sockfd,fd);	/* do it all */
	
	i=write(fd, msg3, sizeof (msg3));

	exit(0);	


}
