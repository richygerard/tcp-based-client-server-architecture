#define MAX_PAYLOAD_LEN (512 - 5 * sizeof(unsigned))

struct reliable_udp_packet {
    unsigned type;
    unsigned sequence;
    unsigned payload_len;
    unsigned acknowledgement;
    unsigned window;
    unsigned timestamp;
    char payload[MAX_PAYLOAD_LEN];
};

enum {
    SYN_MSG,
    SYN_ACK_MSG,
    DATA_ACK_MSG,
    DATA_PCKT,
    FIN_MSG,
    FIN_ACK_MSG,
    PROBE_WIN_MSG
};
