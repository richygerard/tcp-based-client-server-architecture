/* HEADER FILES */
#include "unp.h"
#include <fcntl.h>
#include <sys/file.h>

/* For Inetd */
extern int daemon_proc();
	
/* Global for both threads to access */
void *copyto(void *);
static int sockfd;		
static FILE *fp;
static void *doit1(void *);
static void *doit2(void *);



/* Str_Cli Function Definition */
void str_cli(FILE *fp_arg, int sockfd_arg)
{
	char		recvline[MAXLINE];
	pthread_t	tid;

	sockfd = sockfd_arg;							/* copy arguments to externals */
	fp = fp_arg;

	Pthread_create(&tid, NULL, copyto, NULL);

	while (Readline(sockfd, recvline, MAXLINE) > 0)
		Fputs(recvline, stdout);
}


/* Copyto Function Definition */
void *copyto(void *arg)
{
	char	sendline[MAXLINE];

	while (Fgets(sendline, MAXLINE, fp) != NULL)
		Writen(sockfd, sendline, strlen(sendline));

	Shutdown(sockfd, SHUT_WR);						/* EOF on stdin, send FIN */

	return(NULL);
										/* Thread Terminates when EOF on stdin */
}

/* Printtime Function Definition */
void printtime(int connfd2)
{
	int rn;
	char buff[1000];
	time_t ticks;
	ticks = time(NULL);
	Pthread_detach(pthread_self());						/* Detaching Thread */
	fd_set rst;
	struct timeval tv;
	
	snprintf(buff, sizeof(buff), " %.24s\r\n", ctime(&ticks));
		Write(connfd2, buff, strlen(buff));				/* Writing to Server */
	
	for(;;)
	{
	FD_ZERO(&rst);								/* Implementing Select for Time */
	FD_SET(connfd2, &rst);

	tv.tv_sec=5;								/* Sleep time given as 5 seconds */
	tv.tv_usec=0;
	rn=select(connfd2+1,&rst, NULL, NULL, &tv);
	if(rn<0) 
	{
		if(errno==EWOULDBLOCK) 
			{
			fprintf(stderr,"\nsocket timeout\n");
			continue;
			}
		else
			err_sys("recvfrom error");
	}

	if(rn==0)
	{
		snprintf(buff, sizeof(buff), " %.24s\r\n", ctime(&ticks));
		Write(connfd2, buff, strlen(buff));
	}
	
	if(FD_ISSET(connfd2,&rst))
	{	
	printf("\n Service Client Thread Terminated \n");
	close(connfd2);
	pthread_exit(NULL);							/* Killing Thread */
	}
	}
}

static void * doit1(void *arg)							/* Select Function for doit1 */
{
	int connfd1;
	pid_t pid1;
	Pthread_detach(pthread_self());
	connfd1=(int)arg;

	printf("\n Echo Service Client Thread Created \n");
	str_echo(connfd1);							/* Calling str_echo() */
	printf("\n Echo Service Client Thread Terminated \n");
	Close(connfd1);
}

static void * doit2(void *arg)							/* Select Function for doit2 */
{
	int connfd2;

	printf("\n Time Service Client Thread Created \n");
	
	connfd2=(int)arg;
	printf("%d",connfd2);
	printtime(connfd2);							/* Calling printtime() */

	Close(connfd2);
}

/* Main Function */
int main(int argc, char **argv)
{
	int i,maxi,maxfdp1,nready,client[FD_SETSIZE],a;
	int listenfd1, listenfd2;
	int connfd1,connfd2;
	int sockfd1,sockfd2;
	int fileflags;
	char buff1[MAXLINE]="",buff2[MAXLINE]="",buff3[MAXLINE]="";
	const int on=1;
	fd_set rset,allset;

	pid_t	pid1,pid2;							/* Process Identifiers */
	socklen_t clilen1,clilen2;
	pthread_t *tid1,*tid2;
	struct sockaddr_in cliaddr1,cliaddr2, servaddr1,servaddr2;
	
	a=system("clear");

	printf("\n----------------------------- ");		
	printf("\nSERVER - TCP TIME ECHO SERVER ");
	printf("\n----------------------------- \n");	

//	daemon_init(argv[0],0);
	
	listenfd1 = Socket(AF_INET, SOCK_STREAM, 0);
	printf("\n Echo Service Socket Created \n");				/* Creating Sockets */
	listenfd2 = Socket(AF_INET, SOCK_STREAM, 0);
	printf("\n Time Service Socket Created \n");
	bzero(&servaddr1, sizeof(servaddr1));
	bzero(&servaddr2, sizeof(servaddr2));
	servaddr1.sin_family      = AF_INET;
	servaddr1.sin_addr.s_addr = htonl(INADDR_ANY);				/* Socket Description */
	servaddr1.sin_port        = htons(49391);

	servaddr2.sin_family      = AF_INET;
	servaddr2.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr2.sin_port        = htons(56868);


	if (fileflags = fcntl(listenfd2, F_GETFL, 0) == -1)  
	{
   		perror("fcntl F_GETFL");
   		exit(1);
  	}

	if (fcntl(listenfd2, F_SETFL, fileflags | FNDELAY) == -1)  
	{
   		perror("fcntl F_SETFL, FNDELAY");
   		exit(1);
  	}
	setsockopt(listenfd1, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)); 	/* Setsockopt() - Setting Socket Option */
	printf("\n REUSEADDR Enabled for Echo Service Socket \n");
	setsockopt(listenfd2, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
	printf("\n REUSEADDR Enabled for Time Service Socket \n");

	Bind(listenfd1, (SA *) &servaddr1, sizeof(servaddr1));			/* Binding Sockets */
	Bind(listenfd2, (SA *) &servaddr2, sizeof(servaddr2));
	Listen(listenfd1, LISTENQ);						/* Listening to Sockets */
	Listen(listenfd2, LISTENQ);

	printf("\n WAITING FOR REQUESTS \n");

	for(i=0;i<FD_SETSIZE; i++)
		client[i]=-1;
	FD_ZERO(&allset);
	FD_SET(listenfd1,&allset);
	FD_SET(listenfd2,&allset);
	

	for ( ; ; ) {								/* Implementing Select for Time & Echo Client */
		rset=allset;
		maxfdp1=max(listenfd1,listenfd2)+1;
		nready=select(maxfdp1, &rset, NULL, NULL, NULL);
		if(FD_ISSET(listenfd1,&rset)) 					/* Echo Service */
		{
		snprintf(buff1,sizeof buff1, "\n Received a Request for Echo Service\n");
		i=write(STDOUT_FILENO,buff1,sizeof buff1);
		
		snprintf(buff2,sizeof buff2, "\n SERVICE RUNNING : ECHO SERVICE\n");
		i=write(STDOUT_FILENO,buff2,sizeof buff2);
		
		clilen1 = sizeof(cliaddr1);
		if((connfd1 = Accept(listenfd1, (SA *) &cliaddr1, &clilen1))<0)	/* Accepting Connection*/
			{
			/*Checking if EINTR was thrown*/
			if(errno==EINTR)
				continue;
			else
				err_sys("\n Error : Accepting Connection Error\n");
			}
		Pthread_create(&tid1, NULL, &doit1, (void *)connfd1);		/* Creating Thread for Echo Service */

		}		
		
		if(FD_ISSET(listenfd2,&rset)) 					/* Echo Service */
		{	
		snprintf(buff1,sizeof buff1, "\n Received a Request for Time Service\n");
		i=write(STDOUT_FILENO,buff1,sizeof buff1);
							
		snprintf(buff2,sizeof buff2, "\n SERVICE RUNNING : TIME SERVICE\n");
		i=write(STDOUT_FILENO,buff2,sizeof buff2);

		clilen2 = sizeof(cliaddr2);
		if((connfd2 = Accept(listenfd2, (SA *) &cliaddr2, &clilen2))<0)	/* Accepting Connection*/
			{
			/*Checking if EINTR was thrown*/
			if(errno==EINTR)
				continue;
			else
				err_sys("\n Error : Accepting Connection Error \n");
			}
	       	Pthread_create(&tid2, NULL, &doit2, (void *)connfd2);		/* Creating Thread for Time Service*/

		}

	}
	printf("\n----------------------------------- ");		
	printf("\nSERVER - TCP TIME ECHO SERVER ENDED");
	printf("\n----------------------------------- ");

	exit(0);		
}



