#include "client.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <math.h>

struct client_info client_args;
struct client_params misc_client_info;

static void recv_r_udp_msg(int sockfd, struct reliable_udp_packet *, struct sockaddr*, socklen_t*);
static void send_r_udp_msg(int, const unsigned, char *, int, struct sockaddr*, socklen_t);
static int determine_interface(struct ifi_info *, struct sockaddr_in *, struct sockaddr_in *);

void sig_alrm_handler(int signo)
{
    siglongjmp(misc_client_info.jmpbuf, 1);

}

int main(int argc, char **argv)
{
    int ret = 0, yes = 1, len = 0;
    int sockfd;
    struct ifi_info *ifihead;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    char ip_addr[INET_ADDRSTRLEN] = {0};

    ret = read_config_file(CLIENT, (void *)&client_args);

    if(ret) {
        fprintf(stderr, "Error in parsing client's configuration file.\n");
        return ret;
    }

    srand(client_args.random_seed);

    Signal(SIGALRM, sig_alrm_handler);
    Signal(SIGPIPE, SIG_IGN);

    sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

    /* Initialize server info */
    memset(&server_addr, 0, sizeof(struct sockaddr_in));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = client_args.server_addr.s_addr;
    server_addr.sin_port = htons(client_args.server_port);

    /* Initialize client info */
    memset(&client_addr, 0, sizeof(struct sockaddr_in));
    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = INADDR_NONE;

    ifihead = Get_ifi_info_plus(AF_INET, 1);

    prifinfo_plus(ifihead);

    ret = determine_interface(ifihead, &client_addr, &server_addr);

    if(ret) {
        /* The server host is 'local' to its (extended) Ethernet */
        Setsockopt(sockfd, SOL_SOCKET, SO_DONTROUTE, &yes, sizeof(yes)); 
        misc_client_info.is_same_subnet = 1;
    }
    free_ifi_info_plus(ifihead);
    ret = 0;

    Bind(sockfd, (SA *)&client_addr, sizeof(client_addr));

    len = sizeof(client_addr);

    if(getsockname(sockfd, (SA *)&client_addr, &len) < 0){
        perror("getsockname");
        return EXIT_FAILURE;
    }
    printf("INFO: Upon bind(), client IP is: %s, and ephemeral port is: %hu\n\n", 
           inet_ntop(AF_INET, &client_addr.sin_addr, ip_addr, INET_ADDRSTRLEN), 
           ntohs(client_addr.sin_port));

    Connect(sockfd, (SA *)&server_addr, sizeof(server_addr)); 

    len = sizeof(server_addr);
    if (getpeername(sockfd, (SA *)&server_addr, &len) < 0) {
        perror("getpeername");
        return EXIT_FAILURE;
    }

    printf("INFO: Upon connect(), server IP is: %s, and the well known port number is: %hu\n\n", 
           inet_ntop(AF_INET, &server_addr.sin_addr, ip_addr, INET_ADDRSTRLEN), 
           ntohs(server_addr.sin_port));

    /* Keep track of which one of the client's interface(s) are we using */
    misc_client_info.client_addr = client_addr.sin_addr;

    receive_file(sockfd, &server_addr, client_args.filename);

    return ret;
}

int determine_interface(struct ifi_info *ifihead, 
                         struct sockaddr_in *client_addr, 
                         struct sockaddr_in *server_addr)
{
    struct ifi_info *ifi = 0;
    int is_loopback = 0, same_subnet = 0;
    struct in_addr network_mask;
    struct in_addr interface_addr;
    struct in_addr client_subnet;
    struct in_addr server_subnet;
    unsigned current_mask = 0, longest_mask = 0;
    char ip[INET_ADDRSTRLEN] = {0};

    for (ifi = ifihead; ifi != NULL; ifi = ifi->ifi_next) {

        interface_addr = ((struct sockaddr_in *) ifi->ifi_addr)->sin_addr;
        network_mask = ((struct sockaddr_in *) ifi->ifi_ntmaddr)->sin_addr;

        if(ifi->ifi_flags & IFF_LOOPBACK) 
            is_loopback = 1;
        else
            is_loopback = 0;

        client_subnet.s_addr = interface_addr.s_addr & network_mask.s_addr;
        server_subnet.s_addr = server_addr->sin_addr.s_addr & network_mask.s_addr;

        /* Check if client and server are on the same subnet */
        if(client_subnet.s_addr == server_subnet.s_addr) {

            same_subnet = 1;

            /* Now, check if client and server reside on the same host */
            if(interface_addr.s_addr == server_addr->sin_addr.s_addr) {

                server_addr->sin_addr.s_addr = INADDR_LOOPBACK;
                client_addr->sin_addr.s_addr = INADDR_LOOPBACK;
                printf("\nMESSAGE: Client and server reside on the same host.\n");
                inet_ntop(AF_INET, &client_addr->sin_addr, ip, INET_ADDRSTRLEN);
                printf("\n--------------------------\n");
                printf("IP Client: %s\n", ip);

                inet_ntop(AF_INET, &server_addr->sin_addr, ip, INET_ADDRSTRLEN);
                printf("IP Server: %s\n", ip);
                printf("--------------------------\n\n");
                return same_subnet;
            }

            else {
                /* Client and server belong to the same subnet, but NOT the
                 * same host. Find the interface which has the longest
                 * prefix matching with the server
                 */
                current_mask = network_mask.s_addr;

                if(longest_mask < current_mask) {
                    longest_mask = current_mask;
                    client_addr->sin_addr.s_addr = interface_addr.s_addr;
                }
            }
        }
        else {
            /* Client and server do not belong to the same subnet; assign the
             * next available non-loopback address to the client
             */

            if(!is_loopback && client_addr->sin_addr.s_addr == INADDR_NONE) {
                client_addr->sin_addr.s_addr = interface_addr.s_addr;
            }
        }
    }
    if(same_subnet)
        printf("Client and server reside on the same subnet.\n");
    else
        printf("Client and server belong to different subnets.\n");

    inet_ntop(AF_INET, &client_addr->sin_addr, ip, INET_ADDRSTRLEN);
    printf("\n-----------------------\n");
    printf("IP Client: %s\n", ip);

    inet_ntop(AF_INET, &server_addr->sin_addr, ip, INET_ADDRSTRLEN);
    printf("IP Server: %s\n", ip);
    printf("-----------------------\n\n");
    return same_subnet;
}

void receive_file(int sockfd, struct sockaddr_in *dest_addr, char *filename)
{
    struct reliable_udp_packet *pkt = malloc(sizeof(*pkt));
    short port_num;
    socklen_t len = sizeof(struct sockaddr_in);
    struct sockaddr_in my_addr, server_addr;
    int new_socket = 0;

    rtt_init(&misc_client_info.rtt_value);
    rtt_newpack(&misc_client_info.rtt_value);
    misc_client_info.sequence = 0;
    misc_client_info.last_ack = 0;
    misc_client_info.last_seq = 0;
    misc_client_info.window = client_args.rx_window_size;
send_again:
    send_r_udp_msg(sockfd, SYN_MSG, filename, strlen(filename), NULL, 0);

    alarm(rtt_start(&misc_client_info.rtt_value)/1000);        /* set alarm for RTO milliseconds */
#ifdef DEBUG
    rtt_debug(&misc_client_info.rtt_value);
#endif
    if (sigsetjmp(misc_client_info.jmpbuf, 1) != 0) {
        if (rtt_timeout(&misc_client_info.rtt_value) < 0)     /* double RTO, retransmitted enough? */
        {
            errno = ETIMEDOUT;
            perror("\nConnection timed out while sending filename to server");
            Close(sockfd);
            exit(ETIMEDOUT);
        }
        goto send_again;        /* retransmit */
    }
    do {
        len = sizeof(struct sockaddr_in);
        recv_r_udp_msg(sockfd, pkt, (SA *)&server_addr, &len);
    } while(pkt->type != SYN_ACK_MSG);

    alarm(0);                   /* stop SIGALRM timer */
    /* calculate & store new RTT estimator values */
    rtt_stop(&misc_client_info.rtt_value, rtt_ts(&misc_client_info.rtt_value) - pkt->timestamp);

#ifdef DEBUG
    dump_packet_contents(pkt);
#endif

    if(pkt->type == SYN_ACK_MSG) {
        sscanf(pkt->payload, "%hu", &port_num);
        misc_client_info.last_ack++;
#ifdef DEBUG
        printf("\nDEBUG: Port number received from server : %hu\n", port_num);
#endif
        new_socket = Socket(AF_INET, SOCK_DGRAM, 0);

        if(misc_client_info.is_same_subnet) {
            int yes = 1;
            Setsockopt(new_socket, SOL_SOCKET, SO_DONTROUTE, &yes, sizeof(int));
        }
        Getsockname(sockfd, (SA *)&my_addr, &len);
        // Close(sockfd);
        Shutdown(sockfd, SHUT_RDWR);

        /* Use the same interface as determined earlier */
        my_addr.sin_addr = misc_client_info.client_addr;

        server_addr.sin_addr = dest_addr->sin_addr;
        server_addr.sin_port = htons(port_num);
        server_addr.sin_family = AF_INET;

        Bind(new_socket, (SA *)&my_addr, sizeof(my_addr));
        Connect(new_socket, (SA *)&server_addr, sizeof(server_addr));
    }
repeat:
    send_r_udp_msg(new_socket, DATA_ACK_MSG, NULL, 0, NULL, 0);

    len = sizeof(struct sockaddr_in);
    recv_r_udp_msg(new_socket, pkt, (SA *)&server_addr, &len);

    if(pkt->type == SYN_ACK_MSG) {
        goto repeat;
    }

#ifdef DEBUG
    dump_packet_contents(pkt);
#endif
    if(pkt->type == DATA_PCKT) {
        dump_packet_contents(pkt);
        goto repeat;
    }

    if(pkt->type == FIN_MSG) {
        send_r_udp_msg(new_socket, FIN_ACK_MSG, NULL, 0, NULL, 0);
        while(1) {
            fd_set fds;
            FD_ZERO(&fds);
            FD_SET(new_socket, &fds);
            struct timeval tv;
            int ret = 0;
            struct reliable_udp_packet pkt;

            tv.tv_sec = 10; /* Wait for 10 secs */
            tv.tv_usec = 0;

#ifdef DEBUG
            printf("\nDEBUG: Enter TIME_WAIT state.\n");
#endif
            FD_ZERO(&fds);
            FD_SET(new_socket, &fds);

            ret = select(new_socket + 1, &fds, NULL, NULL, &tv);

            if(ret < 0)
                Close(new_socket);

            else if(ret == 0) 
                break;

            else {
                if(FD_ISSET(new_socket, &fds)) {
                    recv_r_udp_msg(new_socket, &pkt, NULL, NULL);
                    printf("\nINFO: Duplicate FIN_MSG received from the server\n");
                    send_r_udp_msg(new_socket, FIN_ACK_MSG, NULL, 0, NULL, 0);
                }
            }
        }
        printf("\nINFO: Client is now exiting....\n");
        Shutdown(new_socket, SHUT_RD);
        // Close(new_socket);
        return;
    }
}

static void send_r_udp_msg(int sockfd, const unsigned type, char *payload, int payload_len, struct sockaddr* dest_addr, socklen_t dest_len)
{
    struct reliable_udp_packet packet;
    float prob = rand() % 100;
    prob /= 100;

    packet.type = type;
    packet.sequence = misc_client_info.sequence + 1;
    packet.window = misc_client_info.window;
    packet.payload_len = payload_len;
    packet.acknowledgement = misc_client_info.sequence + 1;
    packet.timestamp = rtt_ts(&misc_client_info.rtt_value);

    memset(packet.payload, 0, MAX_PAYLOAD_LEN);
    if(payload) {
        memcpy(packet.payload, payload, payload_len);
    }
    else
        packet.payload_len = 0;

    memcpy(&misc_client_info.duplicate, &packet, sizeof packet);

    printf("\nINFO: Sent packet with seq. num : %u, and window size : %u\n", packet.sequence, packet.window);
    if(prob <= client_args.loss_probability /* && packet.type != SYN_MSG && packet.type != FIN_ACK_MSG && packet.type != FIN_MSG*/) {
        printf("\nINFO: The following packet with seq. num : %u was dropped\n", packet.sequence);
#ifdef DEBUG
        dump_packet_contents(&packet);
        // printf("DEBUG: Probability is: %f", prob);
#endif
        return;
    }
    Sendto(sockfd, &packet, sizeof(packet), 0, dest_addr, dest_len);
}

static void recv_r_udp_msg(int sockfd, struct reliable_udp_packet *pkt, struct sockaddr* src_addr, socklen_t* src_len)
{
    float prob = rand() % 100;
    prob /= 100;

    Recvfrom(sockfd, pkt, sizeof(*pkt), 0, src_addr, src_len);

    if(pkt->sequence > misc_client_info.last_seq)
        misc_client_info.last_seq = pkt->sequence;

    printf("\nINFO: Received packet with seq. num : %u\n", pkt->sequence);
    if(prob <= client_args.loss_probability /* && packet.type != FIN_ACK_MSG && packet.type != FIN_MSG */) {
        printf("\nINFO: The following packet with seq. num : %u was dropped\n", pkt->sequence);
#ifdef DEBUG
        dump_packet_contents(pkt);
        // printf("DEBUG: Probability is: %f\n", prob);
#endif
        return;
    }

}

