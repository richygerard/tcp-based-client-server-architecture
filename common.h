#ifndef __COMMON_H__
#define __COMMON_H__

#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "r_udp.h"
#include "unp.h"
#include "unprtt.h"
#include <setjmp.h>

#define DEBUG

enum {
    CLIENT,
    SERVER
};

int read_config_file(const int, void *);

unsigned long long get_millisecond(void);

#ifdef DEBUG
void dump_packet_contents(struct reliable_udp_packet *);
#endif
#endif

