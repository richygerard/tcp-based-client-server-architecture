#include "lists.h"

/* All functions have been adopted from the Linux kernel */

inline void INIT_LIST_HEAD(struct list_head *list)
{
    list->next = list;
    list->prev = list;
}

inline void __list_add(struct list_head *new,
                              struct list_head *prev,
                              struct list_head *next)
{
    next->prev = new;
    new->next = next;
    new->prev = prev;
    prev->next = new;
}

inline void __list_del(struct list_head * prev, struct list_head * next)
{
     next->prev = prev;
     prev->next = next;
}

/* For stack implementation */
inline void list_add(struct list_head *new, struct list_head *head)
{
     __list_add(new, head, head->next);
}

/* For queue implementation */
inline void list_add_tail(struct list_head *new, struct list_head *head)
{
    __list_add(new, head->prev, head);
}

/* Check if list is empty */
inline int list_empty(const struct list_head *head)
{
     return head->next == head;
}

/* Delete an entry from the list */
inline void list_del(struct list_head *entry)
{
    __list_del(entry->prev, entry->next);
}

