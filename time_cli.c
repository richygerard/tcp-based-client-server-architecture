/* HEADER FILES */
#include "unp.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void INThandler1(int);								/* Ctrl-D handler           */
void INThandler2(int);								/* Ctrl-C handler           */

/*Signal Handler for SIG_QUIT*/
void  INThandler1(int sig)
{
										/*Ignoring the Signal for SIGQUIT (CTRL-D)*/
	signal(sig, SIG_IGN);
	signal(SIGQUIT, INThandler1);
	printf("\n Ctrl D is Disabled\n");
}

/*Signal Handler for SIG_INT*/
void  INThandler2(int sig)
{
										/*Enabling the Signal fo1r SIGINT (CTRL-C)*/
	signal(sig, SIG_IGN);
	exit(0);
	signal(SIGINT, INThandler2);
	printf("\n Ctrl C is Enabled\n");
}

/* Main Function */
int main(int argc, char **argv)
{
	int i,sockfd, n,fd;
	char recvline[MAXLINE]="",buf1[MAXLINE]="";
	char char1[MAXLINE]="",buf2[MAXLINE]="";
	struct sockaddr_in servaddr;
	struct sigaction sac;
	char msg1[]="\n Socket is Created\n";					
	char msg2[]="\n STATUS : \n";
	char msg3[]="\n Connection Terminated\n";
	char msg4[]="\n Connection Established\n";
	
	if (sigaction(SIGINT, NULL, &sac) == -1) 
	{
	exit(0);
	}	
										/*Signal Handler - Disabling SIGQUIT*/
	signal(SIGQUIT,INThandler1);
										/*Signal Handler - Enabling SIGINT*/
	signal(SIGINT, INThandler2);
	printf("\n----------------------------- ");		
	printf("\n  CLIENT - TCP TIME CLIENT ");
	printf("\n----------------------------- \n");	
	
	fd=atoi(argv[2]);
										/*Writing the message to the client*/
	strcpy(buf1,"\n Client - TCP Time Client Invoked\n");
	
	i=write(fd, buf1, MAXLINE);
	i=write(fd, msg2, sizeof (msg2));
	
	if (argc != 3)								/* Checking if there are 2 arguments */
		err_sys("Errors in Passing Parameters");

	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)			/* Socket Creation */
	{
		err_sys("socket error");
	}
	
	i=write(fd, msg1, sizeof (msg1));

	printf("\n Time Pinging from Server is \n\n");

	bzero(&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port   = htons(56868);
	
										/* daytime server */
	if (inet_pton(AF_INET, argv[1], &servaddr.sin_addr) <= 0)
	{	
		err_quit("inet_pton error for %s", argv[1]);
	}
	
	if (connect(sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)		/* Socket Connection */
	{
		err_sys("connect error");
	}
		i=write(fd, msg4, sizeof (msg4));
	strcpy(buf2,"\n Time Retrieved from Server\n");
	i=write(fd, buf2, MAXLINE);

	while ( (n = read(sockfd, recvline, MAXLINE)) > 0) 			/* Reading Data as Time */
	{
		recvline[n] = 0;	/* null terminate */
		if (fputs(recvline, stdout) == EOF)
			err_sys("fputs error");
	}

		i=write(fd, msg3, sizeof (msg3));
	if(n<=0)								/* IF n <=0, Process Terminated */
	{
	printf("\n Process Terminated");
	}

	if (n < 0)
		err_sys("read error");

	exit(0);
}
