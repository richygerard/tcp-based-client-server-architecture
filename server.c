#include "server.h"
#include <stdlib.h>

struct server_info server_args;

struct list_head server_record_head;
struct list_head child_client_head;
static struct server_params misc_server_info;

static int determine_interface(struct sockaddr_in *, 
                               struct server_sock_record *);

static void recv_r_udp_msg(int sockfd, struct reliable_udp_packet *, struct sockaddr*, socklen_t*);

static void send_r_udp_msg(int, const unsigned, char *, int, struct sockaddr*, socklen_t);

void sigchld_handler(int signo)
{
    pid_t   pid;
    struct list_head *pos;
    struct child_info *node;

    while ((pid = waitpid(-1, NULL, WNOHANG)) > 0) {
        fprintf(stderr, "\nINFO: Child %d terminated\n", pid);
    }
    // Remove node from child_client_head list
    list_for_each(pos, &child_client_head) {
        node = list_entry(pos, struct child_info, next_child);
        if(node->pid == pid) {
            list_del(&node->next_child);
            free(node);
        }
    }
}

void sigalrm_handler(int signo)
{
        siglongjmp(misc_server_info.jmpbuf, 1);
}

int main(int argc, char **argv)
{
    int ret = 0;
    int maxfd;
    struct ifi_info *ifihead;

    ret = read_config_file(SERVER, (void *)&server_args);

    if(ret) {
        fprintf(stderr, "Error in parsing server's configuration file.\n");
        return ret;
    }

    Signal(SIGPIPE, SIG_IGN);
    Signal(SIGCHLD, sigchld_handler);

    INIT_LIST_HEAD(&server_record_head);
    INIT_LIST_HEAD(&child_client_head);

    ifihead = Get_ifi_info_plus(AF_INET, 1);

    prifinfo_plus(ifihead);

    maxfd = bind_interfaces(ifihead, server_args.ftp_port);

#ifdef DEBUG
    printf("\nDEBUG: maxfd is : %d\n", maxfd);
#endif
    handle_incoming_connections(maxfd);

    return ret;
}

int bind_interfaces(struct ifi_info *ifihead, in_port_t ftp_port)
{
    struct ifi_info *ifi;
    struct server_sock_record *new_node = NULL;
    struct sockaddr_in serv_addr;
    int yes = 1;
    int maxfd = 0;

    for (ifi = ifihead; ifi != NULL; ifi = ifi->ifi_next) {
        if(ifi->ifi_addr) {

            new_node = malloc(sizeof(*new_node));

            new_node->sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

            if(new_node->sockfd > maxfd)
                maxfd = new_node->sockfd;

            Setsockopt(new_node->sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)); 

            new_node->interface_addr = ((struct sockaddr_in *) ifi->ifi_addr)->sin_addr;
            new_node->network_mask = ((struct sockaddr_in *) ifi->ifi_ntmaddr)->sin_addr;
            new_node->subnet_addr.s_addr= new_node->interface_addr.s_addr & new_node->network_mask.s_addr;

            memset(&serv_addr, 0, sizeof(serv_addr));

            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = new_node->interface_addr.s_addr;
            serv_addr.sin_port = htons(ftp_port);

            Bind(new_node->sockfd, (SA *)&serv_addr, sizeof(serv_addr));
            list_add_tail(&new_node->next_sock_record, &server_record_head);
#ifdef DEBUG
            printf("\nDEBUG: Done binding : %d\n", new_node->sockfd);
#endif
        }

    }
    /* Return the max socket descriptor which will be used by select() */
    return maxfd; 
}

void handle_incoming_connections(int maxfd)
{
    struct list_head *pos;
    fd_set rdset;
    struct server_sock_record *node;

    for(;;) {
loop_forever:
        FD_ZERO(&rdset);
        list_for_each(pos, &server_record_head) {
            node = list_entry(pos, struct server_sock_record, next_sock_record);
#ifdef DEBUG
            printf("\nDEBUG: Socket #%d set in rdset\n", node->sockfd);
#endif
            FD_SET(node->sockfd, &rdset);
        }

        if(select(maxfd + 1, &rdset, NULL, NULL, NULL) >= 0) {
            list_for_each(pos, &server_record_head) {
                node = list_entry(pos, struct server_sock_record, next_sock_record);

                if(FD_ISSET(node->sockfd, &rdset)) {
#ifdef DEBUG
                    printf("\nDEBUG: %d is set.\n", node->sockfd);
#endif
                    handle_client_request(node);
                    printf("\nDEBUG: Done handling a client.\n");
                    break;
                }
            }
        }
        else {
            if(errno == EINTR) {
                goto loop_forever;
            } else
                return;
        }
    }
}

void handle_client_request(struct server_sock_record *listening_socket)
{
    pid_t pid;
    struct reliable_udp_packet msg_from_client;
    socklen_t addrlen = sizeof(struct sockaddr_in);
    struct child_info *new_connection = malloc(sizeof(*new_connection));
    struct sockaddr_in temp;
    struct list_head *pos;
    struct child_info *node;

    recv_r_udp_msg(listening_socket->sockfd, &msg_from_client,  
                   (SA *)&temp, &addrlen);
#ifdef DEBUG
    dump_packet_contents(&msg_from_client);
#endif

    memcpy(&new_connection->client_info, &temp, addrlen);

    if(msg_from_client.type == SYN_MSG) {
        // Check if new message or retransmission before fork()ing
        list_for_each(pos, &child_client_head) {
            node = list_entry(pos, struct child_info, next_child);
            if(node->client_info.sin_addr.s_addr == new_connection->client_info.sin_addr.s_addr) {
                if(node->client_info.sin_port == new_connection->client_info.sin_port) {
                    printf("\nINFO: Duplicate request received, child process with PID %u is already servicing this request\n",
                           node->pid);
                    free(new_connection);
                    return;
                }
            }
        }
    }

    pid = fork();

    if(pid > 0) { /* In parent */
        new_connection->pid = pid;

#ifdef DEBUG
        printf("\nDEBUG: Child %d is handling the client\n", (int)pid);
#endif
        list_add_tail(&new_connection->next_child, &child_client_head);
        return;
    }

    else if(pid == 0) {
        char file_name[30] = {0};
        struct list_head *pos;
        struct server_sock_record *node;
        int connection_socket;
        short port_child;
        int ret;
        struct sockaddr_in child_addr;
        socklen_t len = sizeof(struct sockaddr_in);
        char ip[INET_ADDRSTRLEN] = {0};
        // struct reliable_udp_packet msg_to_client;
        char port_string[6] = {0};
        struct sockaddr_in client_addr;

        /* Close sockets inherited from parent */
        list_for_each(pos, &server_record_head) {
            node = list_entry(pos, struct server_sock_record, next_sock_record);

            if(node->sockfd != listening_socket->sockfd) {
                Close(node->sockfd);
#ifdef DEBUG
                printf("\nDEBUG: Closing sockfd #%d....", node->sockfd);
#endif
                list_del(&node->next_sock_record);
                free(node);
            }
        }
        strncpy(file_name, msg_from_client.payload, msg_from_client.payload_len);
        printf("\nINFO: File to be sent to client : %s\n", file_name);
#if 0

#endif
        // Setup child server with default initial values
        Signal(SIGALRM, sigalrm_handler);

        rtt_init(&misc_server_info.rtt_value);
        rtt_init(&misc_server_info.persist_rtt_value);
        misc_server_info.sequence = 0;
        misc_server_info.window = 1;
        misc_server_info.congestion_window = 1;
        misc_server_info.receiver_window = 1;
        misc_server_info.last_ack = msg_from_client.sequence;
        misc_server_info.last_seq = 0;

        connection_socket = Socket(AF_INET, SOCK_DGRAM, 0);
        ret = determine_interface(&new_connection->client_info, listening_socket); 

        if(ret == LOCAL || ret == SAME_SUBNET) {
            int yes = 1;
            Setsockopt(connection_socket, SOL_SOCKET, SO_DONTROUTE, &yes, sizeof(yes));
        }

        child_addr.sin_family = AF_INET;
        child_addr.sin_port = 0;
        child_addr.sin_addr.s_addr = listening_socket->interface_addr.s_addr;

        Bind(connection_socket, (SA *)&child_addr, len);
        Getsockname(connection_socket, (SA *)&child_addr, &len);

        printf("Upon bind(), server IP : %s, and port is : %hu\n", 
               inet_ntop(AF_INET, &child_addr.sin_addr, ip, INET_ADDRSTRLEN), 
               port_child = ntohs(child_addr.sin_port));

        rtt_newpack(&misc_server_info.rtt_value);


send_again:
        /* Send the new ephemeral port to the client */
        printf("\nINFO: Sending ephemeral port %hu to client.\n", port_child);
        snprintf(port_string, 6, "%hu", port_child);

        send_r_udp_msg(listening_socket->sockfd, SYN_ACK_MSG, port_string, strlen(port_string), 
                       (SA *)&new_connection->client_info, sizeof(client_addr));
        if(misc_server_info.rtt_value.rtt_nrexmt > 0) {
            /* Send ephemeral port to "connection socket" as well */
            send_r_udp_msg(connection_socket, SYN_ACK_MSG, port_string, strlen(port_string),
                           (SA *)&new_connection->client_info, sizeof(client_addr));
        }

        alarm(rtt_start(&misc_server_info.rtt_value)/1000);        /* set alarm for RTO milliseconds */

        if (sigsetjmp(misc_server_info.jmpbuf, 1) != 0) {
            if (rtt_timeout(&misc_server_info.rtt_value) < 0)     /* double RTO, retransmitted enough? */
            {
                errno = ETIMEDOUT;
                perror("\nConnection timed out while sending ephemeral port to client");
                exit(ETIMEDOUT);
            }
            goto send_again;        /* retransmit */
        }

        do {
            len = sizeof(struct sockaddr_in);
            recv_r_udp_msg(connection_socket, &msg_from_client, (SA *)&client_addr, &len);
        } while(msg_from_client.type != DATA_ACK_MSG);

        alarm(0);                   /* stop SIGALRM timer */
        /* calculate & store new RTT estimator values */
        rtt_stop(&misc_server_info.rtt_value, rtt_ts(&misc_server_info.rtt_value) - msg_from_client.timestamp);

        if(msg_from_client.type == DATA_ACK_MSG) {
            struct sockaddr_in sockname;
            struct sockaddr_in peername;
            socklen_t addrlen;
            char sock_name_ip[INET_ADDRSTRLEN + 1] = {0};
            char peer_name_ip[INET_ADDRSTRLEN + 1] = {0};

            Close(listening_socket->sockfd);
            Connect(connection_socket, (SA *)&client_addr, sizeof(struct sockaddr_in));

            Getsockname(connection_socket, (SA *)&sockname, &addrlen);
            Inet_ntop(AF_INET, &sockname.sin_addr, sock_name_ip, INET_ADDRSTRLEN);

            Getpeername(connection_socket, (SA *)&peername, &addrlen);
            Inet_ntop(AF_INET, &peername.sin_addr, peer_name_ip, INET_ADDRSTRLEN);
            // print_peername(connection_socket);
            // print_sockname(connection_socket);

            printf("\nINFO: File transfer will take place between %s:%hu and, %s:%hu\n", sock_name_ip, 
                   ntohs(sockname.sin_port), peer_name_ip, ntohs(peername.sin_port));
            printf("\nINFO: The file to be transferred is : %s\n", file_name);

            misc_server_info.ssthresh = max(2, msg_from_client.window);
            printf("\nINFO: Slow-start threshold set to %u\n", misc_server_info.ssthresh);

        }

        transfer_file(connection_socket, file_name);
        return;
    }
}

int determine_interface(struct sockaddr_in *client, struct server_sock_record *server)
{
#ifdef DEBUG
    char ip[INET_ADDRSTRLEN] = {0};
    Inet_ntop(AF_INET, &client->sin_addr, ip, INET_ADDRSTRLEN);
    printf("\n\nDEBUG: Client's IP is : %s\n", ip);
    Inet_ntop(AF_INET, &server->interface_addr, ip, INET_ADDRSTRLEN);
    printf("\nDEBUG: Listening socket IP is : %s\n", ip);
#endif
    if(client->sin_addr.s_addr == server->interface_addr.s_addr) {
        printf("\nINFO: The server and client are on the same host.\n");
        return LOCAL;
    }

    else if((client->sin_addr.s_addr & server->network_mask.s_addr) == server->subnet_addr.s_addr) {
        printf("\nINFO: The server and client belong to the same subnet.\n");
        return SAME_SUBNET;
    }

    else 
        printf("\nINFO: The server and client belong to different subnets.\n");
    return DIFFERENT_SUBNET;
}

void transfer_file(int sockfd, char *file_name)
{
    char payload[MAX_PAYLOAD_LEN] = {0};
    int nbytes = 0;
    int len = sizeof(struct sockaddr_in);
    struct reliable_udp_packet recvd_packet;
    struct sockaddr_in ipclient;

    FILE *fp = fopen(file_name, "r");

again:
    misc_server_info.window = min(misc_server_info.receiver_window, 
                                  misc_server_info.congestion_window);

    if(misc_server_info.window == 0) {
        /* Send probe messages */
        printf("\nINFO: Entering persist mode\n");
        misc_server_info.persist_mode = 1;
        struct timeval tv;
        fd_set read_fds;
        int ret;

        FD_ZERO(&read_fds);
        FD_SET(sockfd, &read_fds);
        tv.tv_sec = 3;
        tv.tv_usec = 0;
redo_select:
        ret = select(sockfd+1, &read_fds, NULL, NULL, &tv);

        if(ret < 0) {
            if(errno == EINTR)
                goto redo_select;
        }
        else {
            perror("select");
            Shutdown(sockfd, SHUT_WR);
            return;
        }

        if(FD_ISSET(sockfd, &read_fds)) {
            /* Received a packet containing window size */

            recv_r_udp_msg(sockfd, &recvd_packet, (SA *)&ipclient, &len);
            if(recvd_packet.type == PROBE_WIN_MSG) {
                misc_server_info.receiver_window = recvd_packet.window;

            }
        }
        else if(ret == 0) {
            printf("\nINFO: Sending window probe message to client\n");
            send_r_udp_msg(sockfd, PROBE_WIN_MSG, NULL, 0, NULL, 0);
        }
        goto again;
    }
    else { 
        /* Proceed with transferring the file, if it exists */
        misc_server_info.persist_mode = 0;

        rtt_newpack(&misc_server_info.rtt_value);

        if(fp == NULL) {
            strncpy(payload, file_name, nbytes = strlen(file_name));
            goto send_file_missing;
        }

send_data_again:
        while(fp && !feof(fp) && (misc_server_info.window > 0)) {
            nbytes = fread(payload, sizeof(char), MAX_PAYLOAD_LEN, fp);
send_file_missing:
            send_r_udp_msg(sockfd, DATA_PCKT, payload, nbytes, NULL, 0);
            alarm(rtt_start(&misc_server_info.rtt_value)/1000);        /* set alarm for RTO milliseconds */
            if (sigsetjmp(misc_server_info.jmpbuf, 1) != 0) {
                if (rtt_timeout(&misc_server_info.rtt_value) < 0)     /* double RTO, retransmitted enough? */
                {
                    errno = ETIMEDOUT;
                    perror("Connection timed out while sending the file to client");
                    Shutdown(sockfd, SHUT_WR);
                    exit(EXIT_FAILURE);
                }
                if(fp == NULL) {
                    goto send_file_missing;
                }
                else
                    goto send_data_again;        /* retransmit */
            }

            do {
                len = sizeof(struct sockaddr_in);
                recv_r_udp_msg(sockfd, &recvd_packet, (SA *)&ipclient, &len);
            } while(recvd_packet.type != DATA_ACK_MSG);

            alarm(0);                   /* stop SIGALRM timer */
            /* calculate & store new RTT estimator values */
            rtt_stop(&misc_server_info.rtt_value, rtt_ts(&misc_server_info.rtt_value) - recvd_packet.timestamp);

#ifdef DEBUG
            dump_packet_contents(&recvd_packet);
#endif
        }
send_fin:
        if(feof(fp)) {
            send_r_udp_msg(sockfd, FIN_MSG, NULL, 0, NULL, 0);
            alarm(rtt_start(&misc_server_info.rtt_value)/1000);        /* set alarm for RTO milliseconds */
            if (sigsetjmp(misc_server_info.jmpbuf, 1) != 0) {
                if (rtt_timeout(&misc_server_info.rtt_value) < 0)     /* double RTO, retransmitted enough? */
                {
                    errno = ETIMEDOUT;
                    perror("\nConnection timed out while attempting to send FIN message\n");
                    Shutdown(sockfd, SHUT_WR);
                    return;
                }
                goto send_fin;
            }
            do {
                len = sizeof(struct sockaddr_in);
                recv_r_udp_msg(sockfd, &recvd_packet, (SA *)&ipclient, &len);
            } while (recvd_packet.type != FIN_ACK_MSG);
            alarm(0);
            rtt_stop(&misc_server_info.rtt_value, rtt_ts(&misc_server_info.rtt_value) - recvd_packet.timestamp);
        }


#ifdef DEBUG
        dump_packet_contents(&recvd_packet);
#endif
        if(recvd_packet.type == FIN_ACK_MSG) {
            printf("INFO: File successfully sent!\n");
            Close(sockfd);
            return;
        }
    }
}

static void send_r_udp_msg(int sockfd, const unsigned type, char *payload, int payload_len, struct sockaddr* dest_addr, socklen_t dest_len)
{
    struct reliable_udp_packet packet;

    packet.type = type;
    packet.sequence = misc_server_info.sequence + 1;
    packet.window = misc_server_info.window;
    packet.acknowledgement = misc_server_info.last_seq + 1;
    packet.payload_len = payload_len;
    packet.timestamp = rtt_ts(&misc_server_info.rtt_value);

    memset(packet.payload, 0, MAX_PAYLOAD_LEN);
    if(payload) {
        memcpy(packet.payload, payload, payload_len);
    }
    else
        packet.payload_len = 0;

    memcpy(&misc_server_info.duplicate, &packet, sizeof packet);

    printf("\nINFO: Sent packet with seq. num : %u, and window size : %u\n", packet.sequence, packet.window);

    Sendto(sockfd, &packet, sizeof(packet), 0, dest_addr, dest_len);
}

static void recv_r_udp_msg(int sockfd, struct reliable_udp_packet *pkt, struct sockaddr* src_addr, socklen_t* src_len)
{
    Recvfrom(sockfd, pkt, sizeof(*pkt), 0, src_addr, src_len);

    printf("\nINFO: Received packet with seq. num : %u\n", pkt->sequence);
    misc_server_info.last_seq = pkt->sequence;

}

