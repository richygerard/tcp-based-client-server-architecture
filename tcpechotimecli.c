/* HEADER FILE */
#include	"unp.h"


/* To prevent Zombies from escaping - Signal Handling Mechanism */
void sig_chld(int signo)
{
	pid_t	pid;
	int stat;
	while ( (pid = waitpid(-1, &stat, WNOHANG)) > 0)
		printf("\n\n Child %d Terminated Correctly\n", pid);
	return;
}


/* Signal Handler */
void sig_handler(int sig)
{
	 printf("\n EINTR was Handled Successfully");
	
}


/* Main Function */
int main(int argc, char **argv)
{

	int sockfd, n, i,a;
	int ch,c,nread;
	int SIZE=MAXLINE;
	int pfd[2],pfe[2];
	int ret;
	int chkinput_count=0;	
	int pthreadid;
	char recvline[MAXLINE + 1];
	char input[30], *chkinput,star[]="*";
	char buf1[MAXLINE]="",buf2[MAXLINE]="";
	char string[SIZE];
	char writefd1[MAXLINE]="",writefd2[MAXLINE]=""; 	/*Descriptors*/
	pid_t pid1, pid2; 					/* Process IDs*/


	struct sockaddr_in servaddr;
	struct hostent *h1, *h2;
	struct in_addr **addr_list;
	
	
	Signal(SIGCHLD, sig_chld);				/*Calling waitpid to not to leave Zombies*/
	struct sigaction sa; 					/*Signal Handler*/



	void sig_handler(int sig);
	sa.sa_handler = sig_handler;				/* Signal Handler Description */
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if(argc!=2)						/*Invalid Command Line Argument*/
	{
	INV_CMD:
		printf("\n Invalid Command Line Argument. Enter the command line argument :\n ");
		a=scanf(" %s",argv[1]);
	}

	while((h1 = gethostbyname(argv[1])) == NULL) 		/*Invalid Command Line Argument*/
	{  
		goto INV_CMD;
	}

	
	h1 = gethostbyname(argv[1]);				/*Printing Info about the host - gethostbyname()*/
    	printf("\nGiven Input : %s", h1->h_name);
   	printf("\nServer IP address  : ");
   	addr_list = (struct in_addr **)h1->h_addr_list;
    	for(i = 0; addr_list[i] != NULL; i++) {
        printf("%s ", inet_ntoa(*addr_list[i]));
	printf("\n"); 
}

	if (inet_pton(AF_INET, inet_ntoa(*addr_list[0]), &servaddr.sin_addr) <= 0) 
	{
		printf("Invalid Command Line Argument : %s", argv[1]);
		goto INV_CMD;
	}

	if((h2=gethostbyaddr(&servaddr.sin_addr,sizeof servaddr.sin_addr,AF_INET))==NULL)		/*Using gethostbyadr() */
	{
	goto INV_CMD;
	}


	printf("Server Host name   : %s",h2->h_name);		/* Printing Host Name	*/

	if((pipe(pfd))==-1)					/*If Creation of pipe is failed*/
	{
	perror("Error : Creation of Pipe 1 is failed");
	exit(1);
	}


	if((pipe(pfe))==-1)					/*If Creation of pipe is failed*/
	{
	perror("Error : Creation of Pipe 2 is failed");
	exit(1);
	}

	for(i=1;i<=20;i++)					/*Choosing a task from the list*/
	{
	sleep(2);
	printf("\n\n----------------------------- ");		
	printf("\nCLIENT - TCP TIME ECHO CLIENT ");
	printf("\n----------------------------- \n");	

	printf(" \nSelect any one task from Below");		/* Menu */
	printf(" \n\t1. Echo Service"); 
	printf(" \n\t2. Time Service");
	printf(" \n\t3. Quit");
	printf(" \n\nPlease enter 1,2 or 3 to perform a task : ");
	a=scanf("%d",&ch);	
	
	
	switch(ch) 
	{
	case 1:
								/*Activating the Time Service */
		a=system("clear");
		printf("\n------------------------- ");		
		printf("\n ACTIVATING ECHO SERVICE");
		printf("\n------------------------- \n");	
	
		printf("\n Executing the Echo Service Client in Xterm \n");

								/*Forking a child pid1*/
		while((pthreadid=(pid1=Fork())) ==-1)
		{
								/*If Creation of Fork is failed*/
			perror("Creation of Fork is failed");
			exit(2);
		} 
		
		if(pthreadid==0)
		{
								/*Pipe - Child Definition*/
								/*Closing the reading pipes*/			
			close(pfd[0]);
								/*Executing the Time Service Client in Xterm with Pipe File Descriptor*/
			sprintf(writefd2, "%d", pfd[1]);		
			execlp("xterm","xterm","-e","./echo_cli",inet_ntoa(servaddr.sin_addr),writefd2,(char *)0); 

								/*Closing the writing pipe*/	
//			close(pfd[1]);	
		}
		
		else
		{
								/*Pipe - Parent Definition*/
								/*Closing the writing pipe*/
			close(pfd[1]);
		
			while (nread =read(pfd[0], buf1, MAXLINE))
			{	
								/*Status message gets printed*/      	
				a=write(STDOUT_FILENO, buf1, nread);
								/*Closing the reading pipes*/	
//				close(pfd[0]);		
			}
		
            		close(pfd[0]);
		Signal(SIGCHLD, sig_chld);
		break;
		}


	case 2:
		
								/*Activating the Time Service */
		a=system("clear");
		printf("\n------------------------- ");		
		printf("\n ACTIVATING TIME SERVICE");
		printf("\n------------------------- \n");
	
		printf("\n Executing the Time Service Client in Xterm \n");
				
								/*Forking a child pid1*/
		if((pthreadid=(pid2=Fork())) ==-1)
		{
								/*If Creation of Fork is failed*/
			perror("\n Creation of Fork is failed");
			exit(2);
		} 
		
		if(pthreadid==0)
		{
								/*Pipe - Child Definition*/
								/*Closing the reading pipes*/			
			close(pfe[0]);
								/*Executing the Time Service Client in Xterm with Pipe File Descriptor*/
			sprintf(writefd1, "%d", pfe[1]);		
			execlp("xterm","xterm","-e","./time_cli",inet_ntoa(servaddr.sin_addr),writefd1,(char *)0); 
	
								/*Closing the writing pipe*/
//			close(pfd[1]);	
		}
		else
		{
								/*Pipe - Parent Definition*/
								/*Closing the writing pipe*/
			close(pfe[1]);
			while (nread =read(pfe[0], buf2, MAXLINE))
			{	
								/*Status message gets printed*/      	
				a=write(STDOUT_FILENO, buf2, nread);
								/*Closing the reading pipes*/	
//				close(pfd[0]);		
			}
			close(pfe[0]);
			Signal(SIGCHLD, sig_chld);
		break;
		}


	case 3:
		a=system("clear");
		printf("\n------------- ");		
		printf("\n  QUITTING ");
		printf("\n------------- \n\n");
								/*Exiting the Program*/		
		exit(1);



	default:
								/*Unaccepted Inputs*/
		printf("\nERROR : PLEASE ENTER EITHER 1, 2 OR 3");
		break;
	}
	}

	exit(0);
}
